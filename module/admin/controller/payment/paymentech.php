<?php
/*
* @package		Chase Payment Gateway
* @copyright	2013 On Call Software LTD www.oncallsoftware.net
* @license		Commercial
*/

class ControllerPaymentPaymentech extends Controller {
	private $error = array(); 
	public function index() {
		$this->load->language('payment/paymentech');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		$this->data['error_count'] = '';	
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('paymentech', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}
		$this->setLanguageValue('heading_title');

		$this->setLanguageValue('text_enabled');
		$this->setLanguageValue('text_disabled');
		$this->setLanguageValue('text_all_zones');
		$this->setLanguageValue('text_yes');
		$this->setLanguageValue('text_no');
		$this->setLanguageValue('text_authorization');
		$this->setLanguageValue('text_verification');
		$this->setLanguageValue('text_sale');
		
		$this->setLanguageValue('entry_username');
		$this->setLanguageValue('entry_password');
		$this->setLanguageValue('entry_signature');
		$this->setLanguageValue('entry_test');
		$this->setLanguageValue('entry_transaction');
		$this->setLanguageValue('entry_order_status_complete');
		$this->setLanguageValue('entry_order_status_success');
		$this->setLanguageValue('entry_order_status_failure');
		$this->setLanguageValue('entry_status');
		$this->setLanguageValue('entry_sort_order');

		$this->setLanguageValue('entry_livegw1');
		$this->setLanguageValue('entry_livegw2');
		$this->setLanguageValue('entry_testgw1');
		$this->setLanguageValue('entry_testgw2');
		$this->setLanguageValue('entry_url_suggestion');
		$this->setLanguageValue('entry_sug_livegw1');
		$this->setLanguageValue('entry_sug_livegw2');
		$this->setLanguageValue('entry_sug_testgw1');
		$this->setLanguageValue('entry_sug_testgw2');
		
		$this->setLanguageValue('entry_test_company_name');
		$this->setLanguageValue('entry_test_group_number');
		$this->setLanguageValue('entry_test_terminal_id');
		$this->setLanguageValue('entry_test_bin_number');

		$this->setLanguageValue('entry_live_company_name');
		$this->setLanguageValue('entry_live_group_number');
		$this->setLanguageValue('entry_live_terminal_id');
		$this->setLanguageValue('entry_live_bin_number');

		$this->setLanguageValue('entry_live_bin_number');

		$this->setLanguageValue('entry_debug_ghost');
		$this->setLanguageValue('entry_debug_xml_in');
		$this->setLanguageValue('entry_debug_xml_out');
		$this->setLanguageValue('entry_debug_logging');
		$this->setLanguageValue('entry_debug_header');


		$this->setLanguageValue('button_save');
		$this->setLanguageValue('button_cancel');
		$this->setLanguageValue('cert_popup');
		$this->setLanguageValue('text_submit_cardsecval');

		$this->setLanguageValue('text_cc_options');
		

		$this->setLanguageValue('tab_general');

		$this->setErrorDataValue('warning');
		$this->setErrorDataValue('username');
		$this->setErrorDataValue('password');		
		$this->setErrorDataValue('signature');
		$this->setErrorDataValue('test_company_name');
		$this->setErrorDataValue('test_terminal_id');
		$this->setErrorDataValue('test_group_number');
		$this->setErrorDataValue('test_bin_number');
		$this->setErrorDataValue('live_company_name');
		$this->setErrorDataValue('live_terminal_id');
		$this->setErrorDataValue('live_group_number');
		$this->setErrorDataValue('live_bin_number');
		$this->setErrorDataValue('livegw1');
		$this->setErrorDataValue('livegw2');
		$this->setErrorDataValue('testgw1');
		$this->setErrorDataValue('testgw2');
		$this->setErrorDataValue('cc_settings');
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_payment'),
		'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
		'href'      => $this->url->link('payment/paymentech', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);
				
		$this->data['action'] = $this->url->link('payment/paymentech', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['loglink'] = $this->url->link('tool/error_log', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['setupLink'] = $this->url->link('setting/setting', 'token=' . $this->session->data['token'], 'SSL');

		// setup data from request or use default values
		$this->setDataValue('paymentech_username');
		$this->setDataValue('paymentech_password');
		$this->setDataValue('paymentech_livegw1');
		$this->setDataValue('paymentech_livegw2');
		$this->setDataValue('paymentech_testgw1');
		$this->setDataValue('paymentech_testgw2');
		$this->setDataValue('paymentech_test');
		$this->setDataValue('paymentech_cert_popup');
		$this->setDataValue('paymentech_submit_cardsecval');
		$this->setDataValue('paymentech_test_company_name');
		$this->setDataValue('paymentech_test_group_number');
		$this->setDataValue('paymentech_test_terminal_id');
		$this->setDataValue('paymentech_test_bin_number');
		$this->setDataValue('paymentech_live_company_name');
		$this->setDataValue('paymentech_live_group_number');
		$this->setDataValue('paymentech_live_terminal_id');
		$this->setDataValue('paymentech_live_bin_number');
		$this->setDataValue('paymentech_method');
		$this->setDataValue('paymentech_order_status_success_id');
		$this->setDataValue('paymentech_order_status_failure_id');
		$this->setDataValue('paymentech_transaction');
		$this->setDataValue('paymentech_notify_success');
		$this->setDataValue('paymentech_notify_failure');
		$this->setDataValue('paymentech_status');
		$this->setDataValue('paymentech_debug_ghost');
		$this->setDataValue('paymentech_debug_xml_out');
		$this->setDataValue('paymentech_debug_xml_in');
		$this->setDataValue('paymentech_debug_header');

		$this->load->model('localisation/order_status');
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		$this->setDataValue('paymentech_sort_status');		

		$this->setDataValue('paymentech_sort_order');
		$this->setDataValue('paymentech_cc_settings', array("VISA"=>1, "MASTERCARD"=>1, "DISCOVER"=>1) );

		$this->template = 'payment/paymentech.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	// look at our error information and add the appropriate data variables. 
	private function setLanguageValue( $configString, $defaultValue = '' ) {
		$val = $this->language->get($configString);
		$this->data[$configString] = ( $val ? $val : $defaultValue );
	}	
	// look at our error information and add the appropriate data variables. 
	private function setErrorDataValue( $configString, $defaultValue = '' ) {
 		if (isset($this->error[$configString])) {
			$this->data['error_' . $configString ] = $this->error[$configString];
		} else {
			$this->data['error_' . $configString ] = $defaultValue;
		}
	}	
	// add data variables the smart way, allow for default values if specified
	private function setDataValue( $configString, $defaultValue = null ) {
		if (isset($this->request->post[$configString])) {
			$this->data[$configString] = $this->request->post[$configString];
		} else {
			$this->data[$configString] = ( $this->config->has($configString) ? $this->config->get($configString) : $defaultValue );
		}
	}
	private function validate() {
		// validate a few things
		if (!$this->user->hasPermission('modify', 'payment/paymentech')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if ( !$this->_iscurlinstalled() ) {
			$this->error['warning'] = $this->language->get('error_enablecurl');
		}
		if (!$this->request->post['paymentech_username']) {
			$this->error['username'] = $this->language->get('error_username');
		}
		if (!$this->request->post['paymentech_password']) {
			$this->error['password'] = $this->language->get('error_password');
		}
		if (!isset($this->request->post['paymentech_cc_settings'])) {
			$this->error['cc_settings'] = $this->language->get('error_cc_settings');
		}
		if (!$this->error) {
			$this->data["error_count"] = '';
			return true;
		} else {
			$this->data["error_count"] = sprintf( $this->language->get('error_count'), count($this->error) );
			return false;
		}	
	}	// ### Checks for presence of the cURL extension.
	private function _iscurlinstalled() {
		if  ( in_array  ('curl', get_loaded_extensions())) {
			return true;
		}
		else{
			return false;
		}
	}

}
?>