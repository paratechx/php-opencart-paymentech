<?php
/*
* @package		Chase Payment Gateway
* @copyright	2013 On Call Software LTD www.oncallsoftware.net
* @license		Commercial
*/
// Heading
$_['heading_title']      = 'Chase Paymentech Orbital Gateway <small>[v.1.6.2]</small>';

// Text 
$_['text_payment']       		= 'Payment';
$_['text_success']       		= 'Success: You have modified Chase Paymentech Orbital Gateway Checkout account details!';
$_['text_paymentech']       	= '<a onclick="window.open(\'http://www.chasepaymentech.com/\');"><img src="http://en.chasepaymentech.ca/images/logo_chasepaymentech.png" alt="Chase Paymentech Orbital Gateway" title="Chase Paymentech Orbital Gateway" style="border: 1px solid #EEEEEE;background-color:#013963;" height="25" /></a>';
$_['text_authorization'] 		= 'Authorization Only';
$_['text_verification'] 		= 'Verification Only';
$_['text_sale']          		= 'Authorize and Capture';
$_['cert_popup']          		= '<strong>Advanced:</strong> Generate XML-Response Popup for Certification Tests?';
$_['text_submit_cardsecval'] 	= 'Submit the XML-CardSecVal ?';
$_['text_cc_options'] 			= 'Which Credit Cards do you want to support ?';

// Entry
$_['entry_username']     = 'Connection Username:';
$_['entry_password']     = 'Connection Password:';
$_['entry_signature']    = 'API Signature:';
$_['entry_test']         = 'Test Mode:<br /><span class="help">Use the live or testing (sandbox) gateway server to process transactions?</span>';
$_['entry_transaction']  = 'Transaction Method:';
$_['entry_total']        = 'Total:<br /><span class="help">The checkout total the order must reach before this payment method becomes active.</span>';
$_['entry_order_status_complete'] = 'Processed-Order Status:';
$_['entry_order_status_success'] = 'Processed-Order Status:';
$_['entry_order_status_failure'] = 'Failed-Order Status:';
$_['entry_geo_zone']     = 'Geo Zone:';
$_['entry_status']       = 'Payment Module Status:';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_livegw1']   	 = 'Primary Live URL:';
$_['entry_livegw2']   	 = 'Secondary Live URL:';
$_['entry_testgw1']   	 = 'Primary Test URL:';
$_['entry_testgw2']   	 = 'Secondary Test URL:';
$_['entry_url_suggestion'] = 'Standard XML URL:';
$_['entry_sug_livegw1']  = 'https://orbital1.paymentech.net';
$_['entry_sug_livegw2']	 = 'https://orbital2.paymentech.net';
$_['entry_sug_testgw1']	 = 'https://orbitalvar1.paymentech.net';
$_['entry_sug_testgw2']	 = 'https://orbitalvar2.paymentech.net';

$_['entry_test_company_name']   	 = 'Test Merchant Company Name:';
$_['entry_test_terminal_id']   	 = 'Test Merchant Terminal Id:';
$_['entry_test_group_number']   	 = 'Test Merchant Group Number:';
$_['entry_test_bin_number']   	 = 'Test Merchant Bin Number:';
$_['entry_live_company_name']   	 = 'Live Merchant Company Name:';
$_['entry_live_terminal_id']   	 = 'Live Merchant Terminal Id:';
$_['entry_live_group_number']   	 = 'Live Merchant Group Number:';
$_['entry_live_bin_number']   	 = 'Live Merchant Bin Number:';

$_['entry_debug_ghost']				= 'Ghost Transmission:<br><span class="help">Surpresses the curl connect call to servers.</span>';
$_['entry_debug_logging']   	 	= 'Logging Level:';
$_['entry_debug_xml_out']   	 	= '- log outgoing XML';
$_['entry_debug_xml_in']   	 		= '- log chase XML responses'; 
$_['entry_debug_header']   	 		= '- print curl headers'; 



// Error
$_['error_count']			 = "Located %d error(s). Please check the settings below for details. ";
$_['error_permission']   = 'Warning: You do not have permission to modify payment Chase Paymentech Orbital Gateway Checkout!';
$_['error_enablecurl']   = 'Warning: You do not have the php CURL module enabled! Talk to your system admin. It is required to submit XML requests with this module.';
$_['error_username']     = 'API Username Required!'; 
$_['error_password']     = 'API Password Required!'; 
$_['error_signature']    = 'API Signature Required!'; 
$_['error_test_company_name'] = 'Company Name Required!';
$_['error_test_terminal_id']  = 'Terminal Id Required!';
$_['error_test_group_number'] = 'Group Number Required!';
$_['error_test_bin_number']   = 'Bin Number Required!';
$_['error_live_company_name'] = 'Company Name Required!';
$_['error_live_terminal_id']  = 'Terminal Id Required!';
$_['error_live_group_number'] = 'Group Number Required!';
$_['error_live_bin_number']   = 'Bin Number Required!';
$_['error_livegw1']   	 = 'Primary Live URL:';
$_['error_livegw2']   	 = 'Secondary Live URL:';
$_['error_testgw1']   	 = 'Primary Test URL:';
$_['error_testgw2']   	 = 'Secondary Test URL:';
$_['error_cc_settings']   	 = 'At least one credit card must be selected!';

?>