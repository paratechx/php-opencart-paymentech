<?php 
/*
* @package		Chase Payment Gateway
* @copyright	2013 On Call Software LTD www.oncallsoftware.net
* @license		Commercial
*/
?>

<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($error_count ) { ?>
  <div class="warning"><?php echo $error_count; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
		<div id="tabs" class="vtabs">
			<a href="#tab-general">General</a>
			<a href="#tab-live">Live Details</a>
			<a href="#tab-test">Test Details</a>
			<?php /*<a href="#tab-profiles">Profile Management</a>*/ ?>
			<a href="#tab-debug">Debug Settings</a>
		</div>
		
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		<div id="tab-general" class="vtabs-content">
			<table class="form">
				<tr>
					<td><?php echo $entry_test; ?></td>
					<td>
						<p>
							<?php if ($paymentech_test) { ?>
							<input type="radio" name="paymentech_test" value="1" checked="checked" />
							<?php echo $text_yes; ?>
							<input type="radio" name="paymentech_test" value="0" />
							<?php echo $text_no; ?>
							<?php } else { ?>
							<input type="radio" name="paymentech_test" value="1" />
							<?php echo $text_yes; ?>
							<input type="radio" name="paymentech_test" value="0" checked="checked" />
							<?php echo $text_no; ?>
							<?php } ?>
						</p>
						<p> <?php echo $cert_popup; ?><input type="checkbox" name="paymentech_cert_popup" value="1" <?php echo ($paymentech_cert_popup?'checked':''); ?> /></p>
					</td>
				</tr>			
				<tr>
					<td><span class="required">*</span> <?php echo $entry_username; ?></td>
					<td><input type="text" name="paymentech_username" value="<?php echo $paymentech_username; ?>" />
					<?php if ($error_username) { ?>
					<span class="error"><?php echo $error_username; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_password; ?></td>
					<td><input type="password" name="paymentech_password" value="<?php echo $paymentech_password; ?>" />
					<?php if ($error_password) { ?>
					<span class="error"><?php echo $error_password; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><?php echo $entry_transaction; ?></td>
					<td>
						<select name="paymentech_transaction">
							<?php $selectStr = 'selected="selected"'; ?>
							<option value="1" <?php echo ($paymentech_transaction == 1 ? $selectStr : '' ); ?> ><?php echo $text_sale; ?></option>
							<option value="0" <?php echo ($paymentech_transaction == 0 ? $selectStr : '' ); ?> ><?php echo $text_authorization; ?></option>
							<option value="2" <?php echo ($paymentech_transaction == 2 ? $selectStr : '' ); ?> ><?php echo $text_verification; ?></option>
						</select></td>
				</tr>
				<tr>
					<td><?php echo $entry_order_status_complete; ?></td>
					<td>
						<span>Please visit your the settings page of your store.</span>
						<br/><span class="help"><a href="<?php echo $setupLink; ?>">Settings -> Options Tab</a></span>
					</td>
				</tr>
				<tr style="display:none;">
					<td><?php echo $entry_order_status_success; ?></td>
					<td><select name="paymentech_order_status_success_id">
						<?php foreach ($order_statuses as $order_status) { ?>
						<?php if ( ( (isset( $paymentech_order_status_success_id ) && $order_status['order_status_id'] == $paymentech_order_status_success_id) ) ||
										 (!isset( $paymentech_order_status_success_id ) && $order_status['name'] == "Processed" )){ ?>
						<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
					<input type="hidden" name="paymentech_notify_success" value="0">
					</td>
				</tr>
				<tr>
					<td><?php echo $entry_order_status_failure; ?></td>
					<td><select name="paymentech_order_status_failure_id">
						<?php foreach ($order_statuses as $order_status) { ?>
						<?php if ( ( (isset( $paymentech_order_status_failure_id ) && $order_status['order_status_id'] == $paymentech_order_status_failure_id) ) ||
										 (!isset( $paymentech_order_status_failure_id ) && $order_status['name'] == "Failed" )){ ?>
						<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
				   <input type="hidden" name="paymentech_notify_failure" value="0">
				</td>
				</tr>
				<tr>
					<td><?php echo $entry_status; ?></td>
					<td><select name="paymentech_status">
						<?php if ($paymentech_status) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
						<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
						<?php } ?>
					</select></td>
				</tr>
				<tr>
					<td><?php echo $entry_sort_order; ?></td>
					<td><input type="text" name="paymentech_sort_order" value="<?php echo $paymentech_sort_order; ?>" size="1" /></td>
				</tr>
				<tr>
					<td><?php echo $text_submit_cardsecval; ?></td>
					<td>
						<p>
							<?php if ($paymentech_submit_cardsecval) { ?>
							<input type="radio" name="paymentech_submit_cardsecval" value="1" checked="checked" />
							<?php echo $text_yes; ?>
							<input type="radio" name="paymentech_submit_cardsecval" value="0" />
							<?php echo $text_no; ?>
							<?php } else { ?>
							<input type="radio" name="paymentech_submit_cardsecval" value="1" />
							<?php echo $text_yes; ?>
							<input type="radio" name="paymentech_submit_cardsecval" value="0" checked="checked" />
							<?php echo $text_no; ?>
							<?php } ?>
						</p>
					</td>
				</tr>				
				<tr>
					<td>
						<?php echo $text_cc_options; ?>
					</td>
					<td>
						<p>
							<input type="checkbox" name="paymentech_cc_settings[VISA]" value="1" <?php echo (isset( $paymentech_cc_settings["VISA"] ) ?'checked':''); ?> />Visa
							<input type="checkbox" name="paymentech_cc_settings[MASTERCARD]" value="1" <?php echo (isset( $paymentech_cc_settings["MASTERCARD"] )?'checked':''); ?> />Mastercard
							<input type="checkbox" name="paymentech_cc_settings[DISCOVER]" value="1" <?php echo (isset( $paymentech_cc_settings["DISCOVER"] )?'checked':''); ?> />Discover Card
							<input type="checkbox" name="paymentech_cc_settings[AMEX]" value="1" <?php echo (isset( $paymentech_cc_settings["AMEX"] )?'checked':''); ?> />American Express
							<?php if ($error_cc_settings) { ?>
							<span class="error"><?php echo $error_cc_settings; ?></span>
							<?php } ?></td>
						</p>
					</td>
				</tr>
			</table>
		</div>
		<div id="tab-live" class="vtabs-content">
			<table class="form">
				<tr>
					<td><span class="required">*</span> <?php echo $entry_live_company_name; ?></td>
					<td><input type="text" name="paymentech_live_company_name" value="<?php echo $paymentech_live_company_name; ?>" />
					<?php if ($error_live_company_name) { ?>
					<span class="error"><?php echo $paymentech_live_company_name; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_live_group_number; ?></td>
					<td><input type="text" name="paymentech_live_group_number" value="<?php echo $paymentech_live_group_number; ?>" />
					<?php if ($error_live_group_number) { ?>
					<span class="error"><?php echo $paymentech_live_group_number; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_live_terminal_id; ?></td>
					<td><input type="text" name="paymentech_live_terminal_id" value="<?php echo $paymentech_live_terminal_id; ?>" />
					<?php if ($error_live_terminal_id) { ?>
					<span class="error"><?php echo $paymentech_live_terminal_id; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_live_bin_number; ?></td>
					<td><input type="text" name="paymentech_live_bin_number" value="<?php echo $paymentech_live_bin_number; ?>" />
					<?php if ($error_live_bin_number) { ?>
					<span class="error"><?php echo $paymentech_live_bin_number; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_livegw1; ?></td>
					<td><input type="text" name="paymentech_livegw1" value="<?php echo ($paymentech_livegw1?$paymentech_livegw1:$entry_sug_livegw1); ?>" size="50"/>
						<p><?php echo $entry_url_suggestion .  $entry_sug_livegw1;; ?></p>
					<?php if ($error_livegw1) { ?>
					<span class="error"><?php echo $paymentech_livegw1; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_livegw2; ?></td>
					<td><input type="text" name="paymentech_livegw2" value="<?php echo ($paymentech_livegw2?$paymentech_livegw2:$entry_sug_livegw2); ?>" size="50"/>
						<p><?php echo $entry_url_suggestion .  $entry_sug_livegw2;; ?></p>
					<?php if ($error_livegw2) { ?>
					<span class="error"><?php echo $paymentech_livegw2; ?></span>
					<?php } ?></td>
				</tr>
			</table>
		</div>
		<div id="tab-test" class="vtabs-content">
			<table class="form">
				<tr>
					<td><span class="required">*</span> <?php echo $entry_testgw1; ?></td>
					<td><input type="text" name="paymentech_testgw1" value="<?php echo ($paymentech_testgw1?$paymentech_testgw1:$entry_sug_testgw1); ?>" size="50"/>
					<p><?php echo $entry_url_suggestion .  $entry_sug_testgw1; ?></p>
					<?php if ($error_testgw1) { ?>
					<span class="error"><?php echo $paymentech_testgw1; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_testgw2; ?></td>
					<td><input type="text" name="paymentech_testgw2" value="<?php echo ($paymentech_testgw2?$paymentech_testgw2:$entry_sug_testgw2); ?>" size="50"/>
					<p><?php echo $entry_url_suggestion .  $entry_sug_testgw2;; ?></p>
					<?php if ($error_testgw2) { ?>
					<span class="error"><?php echo $paymentech_testgw2; ?></span>
					<?php } ?></td>
				</tr>	
				<tr>
					<td><span class="required">*</span> <?php echo $entry_test_company_name; ?></td>
					<td><input type="text" name="paymentech_test_company_name" value="<?php echo $paymentech_test_company_name; ?>" />
					<?php if ($error_test_company_name) { ?>
					<span class="error"><?php echo $paymentech_test_company_name; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_test_group_number; ?></td>
					<td><input type="text" name="paymentech_test_group_number" value="<?php echo $paymentech_test_group_number; ?>" />
					<?php if ($error_test_group_number) { ?>
					<span class="error"><?php echo $paymentech_test_group_number; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_test_terminal_id; ?></td>
					<td><input type="text" name="paymentech_test_terminal_id" value="<?php echo $paymentech_test_terminal_id; ?>" />
					<?php if ($error_test_terminal_id) { ?>
					<span class="error"><?php echo $paymentech_test_terminal_id; ?></span>
					<?php } ?></td>
				</tr>	
				<tr>
					<td><span class="required">*</span> <?php echo $entry_test_bin_number; ?></td>
					<td><input type="text" name="paymentech_test_bin_number" value="<?php echo $paymentech_test_bin_number; ?>" />
					<?php if ($error_test_bin_number) { ?>
					<span class="error"><?php echo $paymentech_test_bin_number; ?></span>
					<?php } ?></td>
				</tr>
			</table>
		</div>
		<div id="tab-debug" class="vtabs-content">
			<table class="form">
				<tbody>
					<tr>
						<td>
							<span class=""></span> <?php echo $entry_debug_ghost; ?>
						</td>
						<td>
							<p>
								<?php if ($paymentech_debug_ghost) { ?>
								<input type="radio" name="paymentech_debug_ghost" value="1" checked="checked" />
								<?php echo $text_yes; ?>
								<input type="radio" name="paymentech_debug_ghost" value="0" />
								<?php echo $text_no; ?>
								<?php } else { ?>
								<input type="radio" name="paymentech_debug_ghost" value="1" />
								<?php echo $text_yes; ?>
								<input type="radio" name="paymentech_debug_ghost" value="0" checked="checked" />
								<?php echo $text_no; ?>
								<?php } ?>
							</p>
						</td>
					</tr>		
					<tr>
						<td>
							<span class=""></span> <?php echo $entry_debug_logging; ?>
							<br/><span class="help"><a href="<?php echo $loglink; ?>">Error Log</a></span>
						</td>
						<td>
							<p>
								<input type="checkbox" name="paymentech_debug_xml_out" value="1" <?php echo ($paymentech_debug_xml_out?'checked':''); ?> /><?php echo $entry_debug_xml_out; ?>
								<br />
								<input type="checkbox" name="paymentech_debug_xml_in" value="1" <?php echo ($paymentech_debug_xml_in?'checked':''); ?> /><?php echo $entry_debug_xml_in; ?>
								<br />
								<input type="checkbox" name="paymentech_debug_header" value="1" <?php echo ($paymentech_debug_header?'checked':''); ?> /><?php echo $entry_debug_header; ?>
							</p>
						</td>
					</tr>		
				</tbody>
			</table>
		</div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
//--></script> 
<?php echo $footer; ?>