<?php
/*
* @package		Chase Payment Gateway
* @copyright	2013 On Call Software LTD www.oncallsoftware.net
* @license		Commercial
*/

class ModelPaymentPaymentech extends Model {
  	public function getMethod($address, $total) { 
		$this->load->language('payment/paymentech');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('paymentech_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
		
		if ($this->config->get('paymentech_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('paymentech_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}	
		
		$method_data = array();
	
		if ($status) {  
      		$method_data = array( 
        		'code'       => 'paymentech',
        		'title'      => $this->language->get('text_title'),
				'sort_order' => $this->config->get('paymentech_sort_order')
      		);
    	}
   
    	return $method_data;
  	}
}
?>