<?php
/*
* @package		Chase Payment Gateway
* @copyright	2013 On Call Software LTD www.oncallsoftware.net
* @license		Commercial
*/
class ControllerPaymentPaymentech extends Controller {
	protected function index() {
    	$this->language->load('payment/paymentech');
		
		$this->data['text_credit_card'] = $this->language->get('text_credit_card');
		$this->data['text_start_date'] = $this->language->get('text_start_date');
		$this->data['text_issue'] = $this->language->get('text_issue');
		$this->data['text_wait'] = $this->language->get('text_wait');
		
		$this->data['entry_cc_type'] = $this->language->get('entry_cc_type');
		$this->data['entry_cc_number'] = $this->language->get('entry_cc_number');
		$this->data['entry_cc_start_date'] = $this->language->get('entry_cc_start_date');
		$this->data['entry_cc_expire_date'] = $this->language->get('entry_cc_expire_date');
		$this->data['entry_cc_cvv2'] = $this->language->get('entry_cc_cvv2');		

		$this->data['button_confirm'] = $this->language->get('button_confirm');		
		$this->data['cards'] = array();
		// define available credit cards -- currently only 4 are supported
		$cc_settings = $this->config->get('paymentech_cc_settings');
		if ( isset( $cc_settings["VISA"] ) ) {
			$this->data['cards'][] = array(
				'text'  => 'Visa', 
				'value' => 'VISA'
			);
		}
		if ( isset( $cc_settings["MASTERCARD"] ) ) {
			$this->data['cards'][] = array(
				'text'  => 'MasterCard', 
				'value' => 'MASTERCARD'
			);
		}
		if ( isset( $cc_settings["AMEX"] ) ) {
			$this->data['cards'][] = array(
				'text'  => 'American Express', 
				'value' => 'AMEX'
			);
		}
		if ( isset( $cc_settings["DISCOVER"] ) ) {
			$this->data['cards'][] = array(
				'text'  => 'Discover Card', 
				'value' => 'DISCOVER'
			);
		}
		// define allowable months for the cc validation
		$this->data['months'] = array();		
		for ($i = 1; $i <= 12; $i++) {
			$this->data['months'][] = array(
				'text'  => strftime('%B', mktime(0, 0, 0, $i, 1, 2000)), 
				'value' => sprintf('%02d', $i)
			);
		}		
		// define allowable years for the cc validation
		$this->data['year_valid'] = array();
		$today = getdate();		
		for ($i = $today['year'] - 10; $i < $today['year'] + 1; $i++) {	
			$this->data['year_valid'][] = array(
				'text'  => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)), 
				'value' => strftime('%Y', mktime(0, 0, 0, 1, 1, $i))
			);
		}
		// define expiration dates
		$this->data['year_expire'] = array();

		for ($i = $today['year']; $i < $today['year'] + 11; $i++) {
			$this->data['year_expire'][] = array(
				'text'  => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)),
				'value' => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)) 
			);
		}

		$this->data['custom'] = $this->session->data['order_id'];

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/paymentech.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/paymentech.tpl';
		} else {
			$this->template = 'default/template/payment/paymentech.tpl';
		}			
		$this->render();		
	}
	protected function getTransactionDetails($order_id, &$validationErrors, $opsMode) {
		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($order_id);
		// see http://download.chasepaymentech.com/docs/orbital/orbital_gateway_xml_specification.pdf
		// for detailed instructions on each field
		$xmlStr = '<?xml version="1.0" encoding="UTF-8"?><Request></Request>';

		$request = new SimpleXMLElement($xmlStr);
		$order = $request->addChild('NewOrder');

		// <OrbitalConnectionUsername>
		$val = $this->config->get('paymentech_username');
		$order->addChild('OrbitalConnectionUsername', $val);

		// <OrbitalConnectionPassword>
		$val = $this->config->get('paymentech_password');
		$order->addChild('OrbitalConnectionPassword', $val);

		// <IndustryType>EC</IndustryType>
		$val = "EC";	// leave as default for now
		$order->addChild('IndustryType', $val);

		// <MessageType> 
		$val = $this->determineMessageType($order_info);
		$order->addChild('MessageType', $val);

		// <BIN> -- the transaction routing definition, either salem, or PNS
		$val = $this->config->get('paymentech_' . $opsMode . '_bin_number');
		$order->addChild('BIN', $val);

		// <MerchantID>
		$val = $this->config->get('paymentech_' . $opsMode . '_group_number');
		$order->addChild('MerchantID', $val);

		// <TerminalID> 
		$val = $this->config->get('paymentech_' . $opsMode . '_terminal_id');
		$order->addChild('TerminalID', $val);

		// <AccountNum> -- the credit card number
		$errornumber = "";
		$errortext = "";
		$ccnumber = preg_replace('/[^\d]/', '', $this->request->post['cc_number']);
		if ( $this->checkCreditCard ( $ccnumber, 
												$this->request->post["cc_type"], 
												$errornumber, $errortext ) ) {
			$order->addChild('AccountNum', $ccnumber);
		} else {
			// use the errno and errtxt to return a message to the user
			$validationErrors[] = "[Error:$errornumber] -- $errortext ";
		}

		// <Exp> -- because we use select boxes, this should already be somewhat cleanly formatted
		$val = $this->request->post['cc_expire_date_month'] . substr($this->request->post['cc_expire_date_year'], 2);
		$order->addChild('Exp', $val);

		// <CurrencyCode>
		$val = $this->translateCurrencyCode($order_info['currency_code']); 
		$order->addChild('CurrencyCode', $val["code"]);

		// <CurrencyExponent> -- usually it's 2
		$order->addChild('CurrencyExponent', $val["exponent"]);

		// <CardSecValInd>
		$val = $this->determineCardSecValInd();
		if ( $val == "1" || $val == "2" || $val == "9" || $val == "NULL" ) $order->addChild('CardSecValInd', $val);

		// <CardSecVal>
		$val = $this->validateCardSecVal();
		if ( $val ) $order->addChild('CardSecVal', $val);

		// <AVSzip>
		$val = $this->validateZipCode( $order_info['payment_postcode'] );
		if ( $val )	{ $order->addChild('AVSzip', $val); }
		else { $validationErrors[] = "Invalid Billing Zip Code "; }

		// <AVSaddress1>
		$val = substr(preg_replace("/[%|^\/]/", "", $order_info['payment_address_1']), 0, 30);
		$order->addChild('AVSaddress1', $val);

		// <AVSaddress2>
		$val = substr(preg_replace("/[%|^\/]/", "", $order_info['payment_address_2']), 0, 30);
		if ( $val ) $order->addChild('AVSaddress2', $val);

		// <AVScity>
		$val = $val = substr(preg_replace("/[%|^\/]/", "", $order_info['payment_city']), 0, 30);
		$order->addChild('AVScity', $val);

		// <AVSstate>
		$val = $this->getAVSstateValue($order_info);
		$order->addChild('AVSstate', $val);

		// <AVSphoneNum>" . $order->bill_telephone . "</AVSphoneNum>
		$val = preg_replace("/[^0-9]/", "", $order_info['telephone']);
		$order->addChild('AVSphoneNum', $val);

		// <OrderID> -- apparently this must be alphanumeric with minimum of 8 characters, so 0-buffer it
		$val = (int)$order_info['order_id'];
		$val = str_pad($val, 8, "0", STR_PAD_LEFT);
		$order->addChild('OrderID', $val);
		
		// <Amount> -- this is dumb, but it uses "implied decimal point. 
		$val = $this->determineTransactionAmount($order_info);
		$order->addChild('Amount', $val);

		return $request->asXML();
	}
	public function process($order_id, $order_status_id) { 
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
	}
	public function send() {
		$this->load->model('checkout/order');

		$order_id = $this->session->data['order_id'];
		
		if (isset($this->request->post['custom']) && !$order_id) {
			$order_id = $this->request->post['custom'];
		} 

		if ( $order_id ) {
			$orderStatusId = 2;	// processing
			$notifyFlag = false;
			$this->process($order_id, $orderStatusId);	// update function only works with a valid status, so we pre-set it to processing
			$this->model_checkout_order->update($order_id, $orderStatusId, "[" . date("H:i:s") . "] \n " . " Attempting to contact payment gateway.", $notifyFlag); // set status to processing
		} else { die("No order-id passed to send() function. Major booboo!"); }

		$opsMode = ( $this->config->get('paymentech_test') ? "test" : "live" );
		$logPrefix = '::[PAYMENTECH-' . uniqid ($order_id . '_') . ']::';
		$merchantId = $this->config->get('paymentech_' . $opsMode . '_group_number');

		// SETUP OF TRANSACTION DETAILS =============
		$validationErrors = array();
		$response_data = array();
		$response_data["ProcStatus"] = -1;
		$strXmlOrderDetails = $this->getTransactionDetails($order_id, $validationErrors, $opsMode);

		// start processing
		if ( count( $validationErrors ) > 0 || ! $merchantId ) {
			if ( !$merchantId ) $validationErrors[] = "Invalid Merchant ID - Please notify the store admin.";
			$response_data["StatusMsg"] = implode("\n", $validationErrors );
		} else {	// attempt a submission

			// modified way to submit header into fields
			// the original single string seems to trip newer php versions - left in here for any issue resolution on older servers

			$splitHeader = array();

			$splitHeader["customrequest"] = "POST";
			$header  = "POST /AUTHORIZE HTTP/1.0\r\n";                //HTTP/1.1 should work fine also

			$splitHeader["fields"] = array();
			$header .= "MIME-Version: 1.0\r\n";
			$splitHeader["fields"][] = "MIME-Version: 1.0";

			// changed from 54 to 56 in latest Chase updates.
			// $header .= "Content-type: application/PTI54\r\n";	
			// $splitHeader["fields"][] = "Content-type: application/PTI54";
			$header .= "Content-type: application/PTI56\r\n";	
			$splitHeader["fields"][] = "Content-type: application/PTI56";

			$header .= "Content-length: " . strlen($strXmlOrderDetails) . "\r\n";
			$splitHeader["fields"][] = "Content-length: " . strlen($strXmlOrderDetails);	

			$header .= "Content-transfer-encoding: text\r\n";
			$splitHeader["fields"][] = "Content-transfer-encoding: text";

			$header .= "Request-number: 1\r\n";
			$splitHeader["fields"][] = "Request-number: 1";

			$header .= "Document-type: Request\r\n";
			$splitHeader["fields"][] = "Document-type: Request";

			$header .= "Merchant-id: " . $merchantId . "\r\n"; // -- removed by sven to test line spacing theory... \r\n";
			$splitHeader["fields"][] = "Merchant-id: " . $merchantId;

			$header .= $strXmlOrderDetails;
			$splitHeader["content"] = $strXmlOrderDetails;

			$header = trim( $header );

			$customHeader = array();
			$customHeader["combined"] = $header;
			$customHeader["split"] = $splitHeader;

			if ( $this->config->get('paymentech_debug_xml_out') ) {
				$this->log->write( $logPrefix . " RequestXML:\n" . $this->secureLogString( $strXmlOrderDetails ));				
			}
			// SETUP OF CONNECTION DETAILS =============
			$url = $this->config->get('paymentech_' . $opsMode . 'gw1');
			if ( $this->config->get('paymentech_debug_ghost') ) {
				if ( $this->config->get('paymentech_debug_header') ) {
					$header = $splitHeader["customrequest"] . "\n" . implode("\n", $splitHeader["fields"] );
					$this->log->write( $logPrefix . " Submitted Curl Header to [Ghost-Mode]: \n" . $this->secureLogString( $header ));
				}

				// we do a fake transmission - use to reduce dummy runs on chase test-servers
				$response_data["ProcStatus"] = -1;
				$response_data["StatusMsg"] = (string) "Ghost Response. Nothing submitted to " . $url . ". Change debug settings to go live!";

			} else {
				// this is the real-deal response
				//$arrResponse = $this->submitCurlRequest( $url, $header );	// comment out next line and uncomment this line to revert to the old ways
				$arrResponse = $this->submitCurlNewRequest( $url, $customHeader["split"], $logPrefix );
				$authorize 		= $arrResponse["response"];
				$curlerrornum 	= $arrResponse["errornum"];
				$curlerror		= $arrResponse["error"];

				// check if we can parse the response as XML
				try { $responseXML = new SimpleXMLElement($authorize); } 
				catch (Exception $e) { 
						$curlerrornum = "-1";
						$curlerror = "Unable to parse a XML response. Check your test/live url settings.";
				}
				// then check out what curl had to say
				if ($curlerrornum) { 
					$response_data["ProcStatus"] = -1;
					$response_data["StatusMsg"] = "A technical problem: $curlerror";
					$this->log->write($logPrefix . ' XML Read Error (cURL) #' . $curlerrornum . '. Description = ' . $curlerror,'error');
					if ( $curlerrornum == -1 ) {
						if ( trim( $authorize ) == '' ) 
							$this->log->write( $logPrefix . ' Could not parse the XML response because it was blank. ' );
						else 
							$this->log->write( $logPrefix . ' Received the following unreadable response:' . $authorize );
					}					
				} else {	// PROCESS CONNECTION RESPONSE DETAILS =============
					if ( $this->config->get('paymentech_debug_xml_in') ) {
						$this->log->write( $this->secureLogString( $logPrefix . ' ResponseXML:' . trim($responseXML->asXML())));
					}
					$response_data = $this->processCurlResponseXML( $order_id, $responseXML, $response_data, $logPrefix );
				}	
			}
		}
		// now setup our ajax response, by default it's json
		$json = array();
		if ( $response_data['ProcStatus'] == 0 && 
			  $response_data['ApprovalStatus'] == 1 ) {	// this is our ideal scenario - we get a zero, so we can gather more data from the response object
			$json = $this->composeJsonSuccessResponse( $order_id, $response_data, $json, $opsMode, $logPrefix );
		} else {
			// wouldn't it be appropriate to update the order with a status and the message?
			$json['error'] = $response_data["StatusMsg"] ."(" . $response_data['ProcStatus'] . ")";
			$message = "Unable to complete transaction:" . $json['error'];
 			$sendNotification = $this->config->get('paymentech_notify_failure');

  			$this->model_checkout_order->update( $order_id, 
  															 $this->config->get('paymentech_order_status_failure_id'), // this is the status someone gets for failed transactions
  															 "[" . date("H:i:s") . "] \n " . $message, 
															 $sendNotification );

			$this->log->write($logPrefix .' Incomplete Paymentech Order #: ' . $order_id . ' . See details in next line.' );
			$this->log->write($logPrefix . ' ' . $json['error']);	
      }
		// send it back now -- this is what ajax pulls up to show the user
		$this->response->setOutput(json_encode($json));	
	}
	protected function determineTransactionAmount($order_info) {
		// Transaction Amount
		// Implied decimal, including those currencies that are a zero
		// exponent. For example, both $100.00 (an exponent of 2) and
		// ¥100 (an exponent of 0) should be sent as
		// <Amount>10000</Amount>.
		$config = $this->config->get("paymentech_transaction" ); 
		switch ( $config  ) {
			case 2: // verification only -- only checks if CC exists and is valid - chase says send zero dollar amount
				return 0;
				break;
			case 0: // authorization -- same as above, but we submit the full authorization value
			default:
			case 1: 	// for sales we put in full value
				return str_replace(".", "", $this->currency->format($order_info['total'], $order_info['currency_code'], false, false));
		} 
	}
	protected function determineMessageType($order_info) {
		// The transaction New Order Transaction Type
		// A 	-  Authorization request
		// AC - Authorization and Mark for Capture
		// FC - Force-Capture request
		// R 	- Refund request
		$val = "";

		$config = $this->config->get("paymentech_transaction" ); 
		switch ( $config  ) {
			case 0: // authorization
			case 2: // verification only 
				return "A";
				break;
			default:
			case 1: 	// authorize and capture
				return "AC";
		} 
		return $val;
	}
	// this pre-validation is on top of chases own validation. will cut down on error traffic sent.
	function checkCreditCard ($cardnumber, $cardname, &$errornumber, &$errortext) {
		// Define the cards we support. You may add additional card types.
		//  Name:      As in the selection box of the form - must be same as user's
		//  Length:    List of possible valid lengths of the card number for the card
		//  prefixes:  List of possible prefixes for the card
		//  checkdigit Boolean to say whether there is a check digit
		$cards = array(  
								array ('name' => 'MasterCard', 
										'length' => '16', 
										'prefixes' => '51,52,53,54,55',
										'checkdigit' => true
										),
								array ('name' => 'VISA', 
										'length' => '16', 
										'prefixes' => '4',
										'checkdigit' => true
										),
								array ('name' => 'AMEX', 
										'length' => '15', 
										'prefixes' => '34,37',
										'checkdigit' => true
										),
								array ('name' => 'Discover', 
	 									'length' => '16', 
	 									'prefixes' => '6011,622,64,65',
	 									'checkdigit' => true
										)
							);
		$ccErrorNo = 0;

		$ccErrors [0] = "Unknown card type";
		$ccErrors [1] = "No card number provided";
		$ccErrors [2] = "Credit card number has invalid format";
		$ccErrors [3] = "Credit card number is invalid";
		$ccErrors [4] = "Credit card number is wrong length";
							
		// Establish card type
		$cardType = -1;
		for ($i=0; $i<sizeof($cards); $i++) {
			// See if it is this card (ignoring the case of the string)
			if (strtolower($cardname) == strtolower($cards[$i]['name'])) {
				$cardType = $i;
				break;
			}
		}		
		// If card type not found, report an error
		if ($cardType == -1) {
			$errornumber = 0;     
			$errortext = $ccErrors [$errornumber];
			return false; 
		}
		// Ensure that the user has provided a credit card number
		if (strlen($cardnumber) == 0)  {
			$errornumber = 1;     
			$errortext = $ccErrors [$errornumber];
			return false; 
		}		
		// Remove any spaces from the credit card number
		$cardNo = str_replace (' ', '', $cardnumber);  
			
		// Check that the number is numeric and of the right sort of length.
		if (!preg_match("/^[0-9]{13,19}$/",$cardNo))  {
			$errornumber = 2;     
			$errortext = $ccErrors [$errornumber];
			return false; 
		}
				
		// Now check the modulus 10 check digit - if required
		if ($cards[$cardType]['checkdigit']) {
			$checksum = 0;                                  // running checksum total
			$mychar = "";                                   // next char to process
			$j = 1;                                         // takes value of 1 or 2		
			// Process each digit one by one starting at the right
			for ($i = strlen($cardNo) - 1; $i >= 0; $i--) {
				// Extract the next digit and multiply by 1 or 2 on alternative digits.      
				$calc = $cardNo{$i} * $j;
				// If the result is in two digits add 1 to the checksum total
				if ($calc > 9) {
				$checksum = $checksum + 1;
				$calc = $calc - 10;
				}
				// Add the units element to the checksum total
				$checksum = $checksum + $calc;
				// Switch the value of j
				if ($j ==1) {$j = 2;} else {$j = 1;};
			} 
			// All done - if checksum is divisible by 10, it is a valid modulus 10.
			// If not, report an error.
			if ($checksum % 10 != 0) {
				$errornumber = 3;     
				$errortext = $ccErrors [$errornumber];
				return false; 
			}
		}  
		// The following are the card-specific checks we undertake.
		// Load an array with the valid prefixes for this card
		$prefix = explode(',',$cards[$cardType]['prefixes']);
		// Now see if any of them match what we have in the card number  
		$PrefixValid = false; 
		for ($i=0; $i<sizeof($prefix); $i++) {
			$exp = '/^' . $prefix[$i] . '/';
			if (preg_match($exp,$cardNo)) {
				$PrefixValid = true;
				break;
			}
		}
		// If it isn't a valid prefix there's no point at looking at the length
		if (!$PrefixValid) {
			$errornumber = 3;     
			$errortext = $ccErrors [$errornumber];
			return false; 
		}
		// See if the length is valid for this card
		$LengthValid = false;
		$lengths = explode(',',$cards[$cardType]['length']);
		for ($j=0; $j<sizeof($lengths); $j++) {
			if (strlen($cardNo) == $lengths[$j]) {
				$LengthValid = true;
				break;
			}
		}
		// See if all is OK by seeing if the length was valid. 
		if (!$LengthValid) {
			$errornumber = 4;     
			$errortext = $ccErrors [$errornumber];
			return false; 
		};   
		// The credit card is in the required format.
		return true;
	}
	protected function translateCurrencyCode($code) {
		$details = array( "exponent" => 2 );
		switch( $code ) {
			case "GBP":
				$details["code"] = 826;
				break;
			case "EUR":
				$details["code"] = 978;
				break;
			case "USD":
				$details["code"] = 840;
				break;
			case "CDN":
			default:
				$details["code"] = 124;
				break;
		}
		return $details;
	}
	protected function determineCardSecValInd() {
		// Card Security Presence Indicator
		// If you are trying to collect a Card Verification Number (CardSecVal) for a Visa or Discover transaction, pass one of these values:
		// 1 Value is Present
		// 2 Value on card but illegible
		// 9 Cardholder states data not available
		// If the transaction is not a Visa or Discover transaction:
		// - Null-fill this attribute OR
		// - Do not submit the attribute at all.
		if ( ! $this->config->get('paymentech_submit_cardsecval') ) {
			return false;
		}
		$cccvv2 = "";
		if ( isset( $this->request->post['cc_cvv2'] ) ) {
			$cccvv2 = $this->request->post['cc_cvv2'];
		} 
		switch( $this->request->post['cc_type'] ) {
			case "DISCOVER":
			case "VISA":
            $digits = 3;
            break;
        default:
				return "NULL";
		}
		if (  (strlen($cccvv2) == $digits)
			&& (strspn($cccvv2, '0123456789') == $digits) ) {
			return 1; // valid number
		} else if ( $cccvv2 == "" ) {
			return 9; // they skipped the value
		} else {
			return 2;	// probably a typo
		}
	}
	protected function validateCardSecVal() {
		// Card Verification Number
		// Visa CVV2 - 3 bytes
		// MasterCard CVC2 - 3 bytes
		// American Express CID - 4 bytes
		// Discover CID - 3 bytes
		// WARNING It is against regulations to store this value.
		if ( ! $this->config->get('paymentech_submit_cardsecval') ) {
			return false;
		}
		$cccvv2 = "";
		if ( isset( $this->request->post['cc_cvv2'] ) ) {
			$cccvv2 = $this->request->post['cc_cvv2'];
		} 
		switch( $this->request->post['cc_type'] ) {
			case "DISCOVER":
			case "VISA":
			case "MASTERCARD":
            $digits = 3;
            break;
        case 'AMEX':
        case 'AMERICANEXPRESS':
        case 'AMERICAN EXPRESS':
            $digits = 4;
            break;
        default:
            return false;
		}
		if (  (strlen($cccvv2) == $digits)
			&& (strspn($cccvv2, '0123456789') == $digits) ) {
			return substr($cccvv2, 0, $digits);
		} else if ( $this->request->post['cc_type'] == 'DISCOVER' || 
						$this->request->post['cc_type'] == 'VISA' ) {
			// special case for these two, as the cardSecValInd requires something to be sent here as well...silly chase...
			if (strlen($cccvv2) >= $digits) {
				return substr($cccvv2, 0, $digits);
			} else {
				return str_pad($cccvv2, $digits, "0");
			}
		}
		return false;
	}
	protected function validateZipCode( $code ) {
		if (
			preg_match('/^\d{5}(?:\-\d{4})?$/i', $code, $matches) OR	// us
			preg_match('/^[a-z]\d[a-z] ?\d[a-z]\d$/i', $code, $matches) OR // can
			preg_match('/^[a-z]{1,2}\d{1,2} ?\d[a-z]{2}$/i', $code, $matches) // uk
		) {
			return $code;
		} else {
			return false;
		}
	}
	protected function getAVSstateValue( $order_info ) {
		// chase has a 2 character limit on the state, so we send back blanks if we're not us or canada
		if ( $order_info['payment_iso_code_2'] == 'US' || 
			  $order_info['payment_iso_code_2'] == 'CA' ) {
			return $order_info['payment_zone_code'];
		} else { 
			return "";
		}
	}
	private function iscurlinstalled() {
		if  ( in_array  ('curl', get_loaded_extensions())) {
			return true;
		}
		else{
			return false;
		}
	}
	private function submitCurlNewRequest( $url, $header, $logPrefix ) {
		if ( $this->iscurlinstalled() ) {
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 20);

			curl_setopt($ch, CURLOPT_VERBOSE, false);

			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $header["customrequest"] );

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2 );

			// debug helper
			curl_setopt($ch, CURLINFO_HEADER_OUT, true);

			curl_setopt($ch, CURLOPT_HTTPHEADER, $header["fields"] );
			curl_setopt($ch, CURLOPT_POSTFIELDS, $header["content"] );

			$authorize = curl_exec($ch); 

			// debug details
			$info = curl_getinfo($ch);
			if ( $this->config->get('paymentech_debug_header') ) {
				$this->log->write( $logPrefix . " Submitted Curl Header to [" . $url . "]: \n" . $this->secureLogString( print_r($info, true) ));
			}
			// Check for curl errors
			$curlerrornum = curl_errno($ch);
			$curlerror = curl_error($ch);
			curl_close ($ch);
		} else {
			$authorize = "<response><error>true</error></response>"; 
			// Check for curl errors
			$curlerrornum = -1;
			$curlerror = "System Error: Unable to load CURL module. Please contact your system admin.";
		}
		return array( 'response' => $authorize, 'errornum' => $curlerrornum, 'error' => $curlerror );
	}
	private function submitCurlRequest( $url, $header ) {
		if ( $this->iscurlinstalled() ) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 20);
			curl_setopt($ch, CURLOPT_HEADER, false);                  // You are providing a header manually so turn off auto header generation
			curl_setopt($ch, CURLOPT_VERBOSE, false);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $header);          // The following two options are necessary to properly set up SSL
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

			$authorize = curl_exec($ch); 

			// Check for curl errors
			$curlerrornum = curl_errno($ch);
			$curlerror = curl_error($ch);
			curl_close ($ch);
		} else {
			$authorize = "<response><error>true</error></response>"; 
			// Check for curl errors
			$curlerrornum = -1;
			$curlerror = "System Error: Unable to load CURL module. Please contact your system admin.";
		}
		return array( 'response' => $authorize, 'errornum' => $curlerrornum, 'error' => $curlerror );
	}
	private function processCurlResponseXML( $order_id, $responseXML, &$response_data, $logPrefix ) {
		// parse the $authorize content into the response_data array
		// done above.
		foreach ($responseXML->children() as $nodeName => $responseDetail) {
			switch( $nodeName ) {
				case 'QuickResp':
				case 'QuickResponse':
					$response_data["ProcStatus"] = intval( $responseDetail->ProcStatus );
					$response_data["StatusMsg"] = (string) $responseDetail->StatusMsg;
					break 2;	// leave our foreach
				case 'NewOrderResp':
					$response_data["ProcStatus"] = intval( $responseDetail->ProcStatus );
					$response_data["MessageType"] = (string) $responseDetail->MessageType;
					$response_data["ApprovalStatus"] = intval( $responseDetail->ApprovalStatus );
					$response_data["StatusMsg"] = (string) $responseDetail->StatusMsg;
					$response_data['AuthCode'] = (string) $responseDetail->AuthCode;
					$response_data['TxRefNum'] = (string) $responseDetail->TxRefNum;
					$response_data['CVV2RespCode'] = (string) $responseDetail->CVV2RespCode;
					$response_data['AVSRespCode'] = (string) $responseDetail->AVSRespCode;
					break 2;	// leave our foreach
				default:
					$response_data["ApprovalStatus"] = -1;
					$response_data["StatusMsg"] = "There was a technical problem. Please contact our customer service.";
					$this->log->write($logPrefix . ' [Error - OrderID:' . $order_id . '] - XML Parse Error: unable to find <QuickResp> or <NewOrderResp> for order id ' . $order_id );
					$this->log->write($logPrefix . ' RequestXML:' . trim($strXmlOrderDetails ));
					$this->log->write($logPrefix . ' ResponseXML:' . trim($responseXML->asXML()) );
					break;	// try if there's another node
			}
		}
		return $response_data;
	}
	private function composeJsonSuccessResponse( $order_id, $response_data, $json, $opsMode, $logPrefix ) {
		$message = '';

		if (isset($response_data['MessageType'])) {
			$message .= 'Successfully '; 
			switch( $response_data['MessageType'] ) {
				case 'A':
					$message .= "authorized";
					break;
				case 'AC':
					$message .= "authorized and captured";
					break;
				default:
					$message .= "completed";
					break;
			}
			$message .= " the transaction.\n";
		}
		// we collect some order details here so that we can log it in the backend for future reference
		if (isset($response_data['AVSRespCode'])) {
			$message .= 'AVSCODE: ' . $response_data['AVSRespCode'] . "\n";
		}
		if (isset($response_data['CVV2RespCode'])) {
			if ( empty( $response_data['CVV2RespCode'] ) ) {
				$response_data['CVV2RespCode'] = " empty ";
			}
			$message .= 'CVV2MATCH: ' . $response_data['CVV2RespCode'] . "\n";
		}
		if (isset($response_data['TxRefNum'])) {
			$message .= 'TRANSACTIONID: ' . $response_data['TxRefNum'] . "\n";
		}

		$sendNotification = false; //$this->config->get('paymentech_notify_success');
		// first we update the history with the transaction details via update
 		$this->model_checkout_order->update( $order_id, 
 														 $this->config->get('paymentech_order_status_success_id'),
 														 "[" . date("H:i:s") . "] \n " . $message, 
														 $sendNotification );

		// then we set our status to nothing so that the confirm function will operate properly
		$this->process( $order_id , 0 );	// this is needed to get the confirmation email sent
		// then we confirm our order which triggers the admin emails.
		$sendNotification = true;
 		$this->model_checkout_order->confirm( $order_id, 
 														  $this->config->get('config_order_status_id'),
 														  "[" . date("H:i:s") . "] \n " . 'Successfully processed your order.', 
														  $sendNotification );


		$json['success'] = $this->url->link('checkout/success');	// all gravy
		if ( $this->config->get('paymentech_cert_popup') && $opsMode == 'test' ) {
			$json['popup-data'] = $message;
		}
		$this->log->write($logPrefix . ' Completed Paymentech Order #: ' . $order_id . ' . See order history for payment gateway response details.' );

		return $json;
	}
	private function secureLogString( $string ) {	// a quick and dirty function to remove data from the log fields that is permitted to be "stored" by regulations
	// i'm talking to you CVV data!
		$matches = array();
		if ( preg_match("/\<CardSecVal\>(\d+)\<\/CardSecVal\>/i", $string, $matches) ) {
			$digitStars = str_pad( "", strlen( trim( $matches[1] ) ), "*" );
			$string = preg_replace("/(\<CardSecVal\>)(\d+)(\<\/CardSecVal\>)/i", "$1" . $digitStars . "$3", $string);
		}
		return $string;
	}
/***
 profile management was in development but chase shut us down to get a test account to do proper test cycles
 so code is on ice for now until a client will provide us with an account that has profile management enabled
 so we can go the whole way

	protected function constructProfileRequestXML() {
		$xmlStr = '<?xml version="1.0" encoding="UTF-8"?><Request></Request>';

		$request = new SimpleXMLElement($xmlStr);
		$order = $request->addChild('Profile');

		// <OrbitalConnectionUsername>
		$val = $this->config->get('paymentech_username');
		$order->addChild('OrbitalConnectionUsername', $val);

		// <OrbitalConnectionPassword>
		$val = $this->config->get('paymentech_password');
		$order->addChild('OrbitalConnectionPassword', $val);

		// <BIN> -- the transaction routing definition, either salem, or PNS
		$val = $this->config->get('paymentech_' . $opsMode . '_bin_number');
		$order->addChild('BIN', $val);

		// <MerchantID>
		$val = $this->config->get('paymentech_' . $opsMode . '_group_number');
		$order->addChild('MerchantID', $val);

		// <CustomerProfileAction>C</CustomerProfileAction>
		$val = "C";
		$order->addChild('CustomerProfileAction', $val);

		// <CustomerProfileOrderOverrideInd>NO</CustomerProfileOrderOverrideInd>
		$val = "NO";
		$order->addChild('CustomerProfileOrderOverrideInd', $val);

		// <CustomerProfileFromOrderInd>S</CustomerProfileFromOrderInd>
		$val = "A";
		$order->addChild('CustomerProfileFromOrderInd', $val);



// CustomerProfileOrderOverrideInd Profile
// Defines if any Order Data can be pre-populated from the
// Customer Reference Number (CustomerRefNum)
//  Mandatory if CustomerProfileAction = C (Create).
//  Optional if CustomerProfileAction = U (Update).
//  Valid Values
// NO
// No mapping to order data
// OI
// OD
// Profile
// Use <CustomerRefNum> for <Comments>
// OA
// CustomerProfileFromOrderInd
// Use <CustomerRefNum> for <OrderID>
// Use <CustomerRefNum> for <OrderID> and
// <Comments>
// Customer Profile Number Generation Options
//  When CustomerProfileAction = C (Create), defines
// what the Customer Profile Number will be:
// A
// Auto-Generate the CustomerRefNum
// S
// Use CustomerRefNum element


// <CustomerName>Jon Doe</CustomerName>
// <CustomerRefNum>ADDPROFILE 123</CustomerRefNum>
// <CustomerAddress1>123 Test Drive</CustomerAddress1>
// <CustomerAddress2>Suite 123</CustomerAddress2>
// <CustomerCity>Test City</CustomerCity>
// <CustomerState>FL</CustomerState>
// <CustomerZIP>33626</CustomerZIP>
// <CustomerEmail>jondoe@test.com</CustomerEmail>
// <CustomerPhone>2232231234</CustomerPhone>
// <CustomerCountryCode>US</CustomerCountryCode>

// <CustomerProfileFromOrderInd>S</CustomerProfileFromOrderInd>
// <OrderDefaultDescription>Sample Order Description</OrderDefaultDescription>
// <OrderDefaultAmount>1500</OrderDefaultAmount>
// <CustomerAccountType>CC</CustomerAccountType>
// <Status>A</Status>
// <CCAccountNum>5454545454545454</CCAccountNum>
// <CCExpireDate>0810</CCExpireDate>

// // // <MBType>R</MBType>
// // // <MBOrderIdGenerationMethod>IO</MBOrderIdGenerationMethod>
// // // <MBRecurringStartDate>11012008</MBRecurringStartDate>
// // // <MBRecurringNoEndDateFlag>Y</MBRecurringNoEndDateFlag>
// // // <MBRecurringFrequency>? * /5 MON</MBRecurringFrequency>

	}
	private function createProfile() {
	// add profile interface
// 		A
// 		Auto Generate the Customer Reference Number (AKA Profile ID, Profile
// 		Number, Token Value). In other words, the Orbital Gateway creates the
// 		Customer Reference Number and returns it in the response message.
// 		S
// 		The Orbital Gateway uses the value passed in the Customer Reference
// 		Number field as the Customer Reference Number (AKA Profile ID, Profile
// 		Number, Token Value).
// 		O
// 		This option only occurs when a Profile is added as a part of an authorization
// 		request. The value passed in the Order ID field is used as the Customer
// 		Reference Number (AKA Profile ID, Profile Number, Token Value). This is
// 		often used when the Order ID also represents the Customer ID in the
// 		merchant’s system, such as a Policy Number for an insurance company.
// 		D
// 		This option only occurs when a Profile is added as a part of an authorization
// 		request. The value passed in the Comments field is used as the Customer
// 		Reference Number (AKA Profile ID, Profile Number, Token Value).
		
	
	}
	private function retrieveProfile($profileId) {
	// retrieve existing profile or decline if not found

	}
	private function deleteProfile($profileId) {

	}
	private function updateProfile($profileId) {
// Customer Information:
// 
// Customer Reference Number – Required and non-editable (Also referred to as Profile ID,
// Profile Number, or Token Value)
// 
// 
// 
// 
// 
// 
// Customer Name
// Customer Email Address
// Address Information:
// Address 1
// Address 2
// City
// 
// 
// 
// 
// State
// ZIP
// Phone
// Amount
// Payment Information:
// 
// 
// Card Type
// Credit Card
// 
// 
// 
// DDA
// Routing/Transit (R/T)
// Account Type
// Payment Delivery Method
// INTERNATIONAL MAESTRO (Salem Host Only – BIN 000001)
// 
// 
// 
// Expiration Date
// ECP (Salem Host Only – BIN 000001)
// 
// 
// 
// 
// 
// Card Number
// Card Number
// Expiration Date
// PINless debit
// 
// 
// 
// Card Number
// Expiration Date
// Biller Reference Number
// Information Not Saved:
// 
// Purchasing Card Data - This information is not stored in accordance with card
// association regulations.
// 
// Card Verification Number [CVV2, CVC2, and CID] - This information is not stored in
// accordance with card association regulations. It must be requested from a cardholder on
// a transaction-by-transaction basis.
// 
// VbV and MasterCard SecureCode Data




	}
***/
}	
?>